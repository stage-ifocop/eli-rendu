import { Component, OnInit } from '@angular/core';

// cet objet permet de relier les 3 fichiers principaux pour la construction du COMPONENT

@Component({
  selector: 'app-article',
  standalone: true,
  imports: [],
  templateUrl: './article.component.html',
  styleUrl: './article.component.css',
})

// ci-dessous , cette classe reste vide la plupart du temps
export class ArticleComponent implements OnInit {
  titre = 'Eli Dev Web';

  constructor() {
    console.log('ICI CONSTRUCTOR article component constructor');
  }

  ngOnInit(): void {
    console.log('ICI article component ngonInit');
  }
}
