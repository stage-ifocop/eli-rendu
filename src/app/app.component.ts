import { Component, OnInit } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { ArticleComponent } from './article/article.component';
import { FooterComponent } from './footer/footer.component';
import { NavbarComponent } from './navbar/navbar.component';
import { CardComponent } from './card/card.component';
import { AproposComponent } from './apropos/apropos.component';

@Component({
  selector: 'app-root',
  standalone: true,
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  imports: [
    RouterOutlet,
    ArticleComponent,
    FooterComponent,
    NavbarComponent,
    CardComponent,
    AproposComponent,
  ],
})
export class AppComponent implements OnInit {
  title = 'eli-rendu';

  constructor() {
    console.log('ICI CONSTRUCTOR app component constructor');
  }

  ngOnInit(): void {
    console.log('ICI app component init');
  }
}
('node_modules/bootstrap/dist/css/bootstrap.css');
